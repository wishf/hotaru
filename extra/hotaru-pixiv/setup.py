from setuptools import setup

setup(name='hotaru-pixiv',
      version='0.0.1',
      description='Pixiv image loader for the Hotaru discord bot',
      author='Matthew Summers',
      author_email='matt@wishf.co.uk',
      license='MIT',
      py_modules=['hotaru_pixiv'],
      install_requires=[
            'pixivpy',
            'hotaru'
      ],
      entry_points={
            'hotaru.watcher': [
                  'watcher-pixiv = hotaru_pixiv:Pixiv',
            ],
      })