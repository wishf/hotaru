from pixivpy3 import *
from hotaru.watcher import Watcher
from io import BytesIO
import requests
import re
from urllib.parse import urlparse
from os.path import basename

class Pixiv(Watcher):
    matcher = re.compile(r"(?:https?://)?(?:www\.)?pixiv\.net/member_illust.php(?:(?:\?|\&)(?:[^=\s]+)\=(?:[^&\s]+))*(?:(?:\?|\&)(?:illust_id)\=(?P<iid>[^&\s]+))(?:(?:\?|\&)(?:[^=\s]+)\=(?:[^&\s]+))*")
    
    
    def __init__(self, registry):
        super(Watcher, self).__init__(registry)
        self._api = None

    async def process(self, message, client):
        if "pixiv.net" in message.content:
            print("potential match", flush=True)
            
            # Build spoof headers
            req_header = {
                'Referer': 'http://spapi.pixiv.net/',
                'User-Agent': 'PixivIOSApp/5.8.3',
            }
            
            req_header['Authorization'] = "Bearer {}".format(self._api.access_token)
            
            for match in Pixiv.matcher.finditer(message.content):
                print("actual match", flush=True)
                json_result = self._api.works(int(match.group("iid")))
                illust = json_result.response[0]
                url = illust.image_urls['medium']
                parsed = basename(urlparse(url).path)[1:]
                resp = requests.get(url, stream=True, headers=req_header)
                f = BytesIO(resp.content)
                await client.send_file(message.channel, f, filename=parsed, content="**{}**".format(illust.title))
    
    @property
    def watches_self(self):
        return False

    def set_config(self, dict):
        if "Username" not in dict or "Password" not in dict:
            raise RuntimeError("No Pixiv username or password given")
        
        username = dict["Username"]
        password = dict["Password"]
        self._api = PixivAPI()
        self._api.login(username, password)
    
    def get_config(self):
        return {}