hotaru-pixiv
============

Pixiv loading plugin for Hotaru

Install in same Python environment as hotaru, adds the watcher plugin 'watcher-pixiv'

Example configuration block below:

```
[Watcher:watcher-pixiv]
Username = test
Password = test
```