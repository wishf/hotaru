hotaru
======

A Discord bot written in Python 3

Depends on: https://github.com/Rapptz/discord.py

Core bot components and default plugins are in the hotaru subdirectory.

Extra plugins are in the extra folder, each subdirectory is a new Python package.
