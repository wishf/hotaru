from pkg_resources import iter_entry_points
import asyncio
from hotaru.watcher import Watcher

class WatcherDispatcher:
    def __init__(self, registry):
        self._registry = registry

    async def process(self, message, client):
        for callback in self._registry.watchers().values():
            if not callback.watches_self and message.author == client.user:
                continue
            
            await callback.process(message, client)