from pkg_resources import iter_entry_points
from hotaru.filter import Filter
from hotaru.persistence import PersistenceProvider
from hotaru.command import Command
from hotaru.watcher import Watcher
from hotaru.overlaydict import OverlayDict
import operator

class Registry:
    def __init__(self, config, default_persistence):
        self._watchers = {}
        self._filters = {}
        self._persistence = {}
        self._commands = {}
        self._sorted_filters = None
        self._default_persistence_name = default_persistence
        self._default_persistence = None
        self._plugin_configs = config
        
        self._reload_plugins()

    def _reload_group(self, klass, group, base, dict):
        dict.clear()
        for entry_point in iter_entry_points(group=group, name=None):
            extension = entry_point.load()
            # Change this to an exception
            assert(issubclass(extension, klass))
            assert(entry_point.name not in dict)
            dict[entry_point.name] = extension(self)
            
            config_name = base + ':' + entry_point.name
            if config_name in self._plugin_configs:
                dict[entry_point.name].set_config(self._plugin_configs[config_name])

    def _reload_plugins(self):
        self._reload_group(Watcher, 'hotaru.watcher', 'Watcher', self._watchers)
        self._reload_group(Filter, 'hotaru.filter', 'Filter', self._filters)
        self._reload_group(PersistenceProvider, 'hotaru.persistence', 'Persistence', self._persistence)
        self._reload_group(Command, 'hotaru.command', 'Command', self._commands)
        
        self._sorted_filters = None
        if self._default_persistence_name in self._persistence:
            self._default_persistence = self._persistence[self._default_persistence_name]
        else:
            raise RuntimeError("Cannot find persistence backend")

    def _resort_filters(self):
        self._sorted_filters = sorted(self._filters.values(), key=operator.attrgetter('priority'))
    
    def set_default_persistence(self, persistence):
        self._default_persistence = persistence
    
    @property
    def default_persistence(self):
        return self._default_persistence
    
    # Don't hold onto this value
    @property
    def filters_by_priority(self):
        if self._sorted_filters is None:
            self._resort_filters()
        return self._sorted_filters
    
    def _overlay(self, dict, overlay=None, enforce_no_occlusion=None):
        if overlay:
            return OverlayDict(dict, overlay, enforce_no_occlusion)
        else:
            return dict
    
    def watchers(self, overlay=None, enforce_no_occlusion=False):
        return self._overlay(self._watchers, overlay, enforce_no_occlusion)

    def filters(self, overlay=None, enforce_no_occlusion=False):
        return self._overlay(self._filters, overlay, enforce_no_occlusion)

    def commands(self, overlay=None, enforce_no_occlusion=False):
        return self._overlay(self._commands, overlay, enforce_no_occlusion)
    