from pkg_resources import iter_entry_points
import asyncio
from hotaru.command import Command

class Dispatcher:
    class DispatcherCommand(Command):
        def __init__(self, method, desc, helptext, registry):
            super(Command, self).__init__(registry)
            self._mth = method
            self._desc = desc
            self._hlp = helptext
        
        @property
        def description(self):
            return self._desc
        
        @property
        def help(self):
            return self._hlp
        
        async def respond(self, message, command, client):
            await self._mth(message, command, client)
         
        def set_config(self, dict):
            pass
        
        def get_config(self):
            return {}


    def __init__(self, invalid_message, registry):
        self._registry = registry
        self._invalid_command_message = invalid_message

        # Inject help function
        self._overlay = {'help': Dispatcher.DispatcherCommand(
            self._help_handler,
            'Displays this message',
            'With a command name, displays the help message for that command\nWithout a command name, displays a list of all commands',
            registry
        )}

    def _get_message_instance(self, prefix):
        overlaid = self._registry.commands(overlay=self._overlay)
        if prefix in overlaid:
            return overlaid[prefix]
        else:
            return None

    def _get_message_callback(self, prefix):
        inst = self._get_message_instance(prefix)
        if inst is None:
            return self._no_handler
        else:
            return inst.respond
    
    async def respond(self, message, client):
        if message.author == client.user:
            return
    
        if message.content.startswith('!'):
            message_parts = message.content[1:].split(' ', 1)
            if len(message_parts) > 1:
                prefix, command = message_parts
            else:
                prefix = message_parts[0]
                command = None
    
            callback = self._get_message_callback(prefix)
            await callback(message, command, client)

    async def _no_handler(self, message, command, client):
        await client.send_message(message.channel, self._invalid_command_message)
    
    async def _help_handler(self, message, command, client):
        if command is None:
            helpmsg = "Available Commands:\n\n"
            for name, inst in self._registry.commands(overlay=self._overlay).items():
                helpmsg = "{}    !{} - {}\n".format(helpmsg, name, inst.description)
            await client.send_message(message.channel, helpmsg)
        else:
            inst = self._get_message_instance(command)
            if inst is None:
                self._no_handler(message, command, client)
            else:
                await client.send_message(message.channel, "!{} - {}\n\n{}".format(command, inst.description, inst.help))