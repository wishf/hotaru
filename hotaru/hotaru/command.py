from abc import ABCMeta, abstractmethod, abstractproperty
from hotaru.plugin import Plugin

class Command(Plugin, metaclass=ABCMeta):
    @abstractproperty
    def description(self):
        pass

    @abstractproperty
    def help(self):
        pass

    @abstractmethod
    async def respond(self, message, command, client):
        pass