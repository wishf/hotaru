from abc import ABCMeta, abstractmethod, abstractproperty

class Plugin(metaclass=ABCMeta):
    def __init__(self, registry):
        self._registry = registry
    
    @abstractmethod
    def set_config(self, dict):
        pass
    
    @abstractmethod
    def get_config(self):
        return {}