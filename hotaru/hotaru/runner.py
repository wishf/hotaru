import configparser
from hotaru.core import setup_client
import logging
import argparse
import asyncio

def main():
    parser = argparse.ArgumentParser(description='Start the Hotaru discord bot')
    parser.add_argument('-c', '--config', dest='config', default='hotaru.ini', help='Specify the config file path')
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true', help='Print more logging information to terminal')
    parser.add_argument('-D', '--debug', dest='debug', action='store_true', help='Enable debug mode')
    
    namespace = parser.parse_args()
    
    config = configparser.ConfigParser()
    config.read(namespace.config, 'utf8')
    
    if namespace.verbose:
        logging.basicConfig(level=logging.INFO)
    elif namespace.debug:
        logging.basicConfig(level=logging.DEBUG)
        asyncio.get_event_loop().set_debug(True)
    
    print([x for x in config.keys()])
    
    client = setup_client(
        config['Core']['InvalidCommand'],
        {x: y for x, y in config.items() if x != 'Core' or x != 'Account' or x != 'DEFAULT'},
        config['Core']['DefaultPersistenceBackend'])
    
    client.run(config['Account']['Username'], config['Account']['Password'])

if __name__ == '__main__':
    main()