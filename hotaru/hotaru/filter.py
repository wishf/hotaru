from abc import ABCMeta, abstractmethod, abstractproperty
from hotaru.plugin import Plugin

class Filter(Plugin, metaclass=ABCMeta):
    @abstractmethod
    def accepts(self, message):
        return message
    
    @abstractproperty
    def priority(self):
        pass