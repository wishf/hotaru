from abc import ABCMeta, abstractmethod, abstractproperty
from hotaru.plugin import Plugin

class PersistenceProvider(Plugin, metaclass=ABCMeta):
    @abstractmethod
    def get(self, primary_id, secondary_id, key, default):
        pass
    
    @abstractmethod
    def set(self, primary_id, secondary_id, key, value):
        pass