import itertools

class OverlayDict:
    # No occlusion is enforced lazily, it is only tested upon access
    def __init__(self, base, overlay, enforce_no_occlusion=False):
        self._enforce = enforce_no_occlusion
        self._overlay = overlay
        self._base = base
    
    def __readonly__(self, *args, **kwargs):
        raise RuntimeError("Cannot modify OverlayDict")
    
    __setitem__ = __readonly__
    __delitem__ = __readonly__
    pop = __readonly__
    popitem = __readonly__
    clear = __readonly__
    update = __readonly__
    setdefault = __readonly__
    
    def items(self):
        for key, value in self._overlay.items():
            if key in self._base:
                raise RuntimeError("Base and overlay layers of OverlayDict occlude")
            yield (key, value)
        
        for item in self._base.items():
            yield item

    def __contains__(self, key):
        return key in self._overlay or key in self._base
    
    def __getitem__(self, key):
        if key in self._overlay:
            if key in self._base and self._enforce:
                raise RuntimeError("Base and overlay layers of OverlayDict occlude")
            return self._overlay[key]
        else:
            return self._base[key]