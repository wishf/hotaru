from pkg_resources import iter_entry_points
import asyncio
from hotaru.filter import Filter
import operator

class Prefilter:
    def __init__(self, registry):
        self._registry = registry
    
    def accepts(self, message):
        for fil in self._registry.filters_by_priority:
            message = fil.accepts(message)
            if message is None:
                return None
        return message