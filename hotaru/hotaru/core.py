import discord
import asyncio
from hotaru.dispatcher import Dispatcher
from hotaru.prefilter import Prefilter
from hotaru.watcher_dispatch import WatcherDispatcher
from hotaru.registry import Registry

def setup_client(invalid_command_message, plugin_configs, default_persistence):
    client = discord.Client()
    registry = Registry(plugin_configs, default_persistence)
    dispatcher = Dispatcher(invalid_command_message, registry)
    prefilter = Prefilter(registry)
    watchers = WatcherDispatcher(registry)

    @client.event
    async def on_ready():
        print("Login complete!")
        print("Name: {}".format(client.user.name))
        print("ID: {}".format(client.user.id))
        print("------", flush=True)
    
    @client.event
    async def on_message(message):
        if prefilter.accepts(message):
            await dispatcher.respond(message, client)
        
            await watchers.process(message, client)
            
    return client