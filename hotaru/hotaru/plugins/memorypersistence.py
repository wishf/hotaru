from hotaru.persistence import PersistenceProvider

class MemoryPersistence(PersistenceProvider):
    def __init__(self, registry):
        super(PersistenceProvider, self).__init__(registry)
        self._dict = {}
    
    def _make_key(self, primary, secondary, key):
        return "{}_{}_{}".format(primary, secondary, key)
    
    def set_config(self, dict):
        pass
    
    def get_config(self):
        return {}

    def get(self, primary_id, secondary_id, key, default):
        return self._dict.get(self._make_key(primary_id, secondary_id, key), default)
    
    def set(self, primary_id, secondary_id, key, value):
        self._dict[self._make_key(primary_id, secondary_id, key)] = value