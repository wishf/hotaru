from hotaru.command import Command

class Remember(Command):
    def __init__(self, registry):
        super(Command, self).__init__(registry)

    @property
    def description(self):
        return "Remember a mesage"

    @property
    def help(self):
        return "\n".join(
            "!remember <string> - Memorise a message",
            "!remember - Recall that message"
        )

    async def respond(self, message, command, client):
        if command is None:
            # We want to retrieve the message
            value = self._registry.default_persistence.get(message.author.id, "remember", "value", None)
            if value is None:
                await client.send_message(message.channel, "You haven't asked me to remember anything")
            else:
                await client.send_message(message.channel, value)
        else:
            # We want to remember the command
            self._registry.default_persistence.set(message.author.id, "remember", "value", command)
            await client.send_message(message.channel, "I will remember \"{}\" for you".format(command))
    
    def set_config(self, dict):
        pass
    
    def get_config(self):
        return {}