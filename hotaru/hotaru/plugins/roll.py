from hotaru.command import Command
import random
import re
from enum import Enum
import operator

class Roll(Command):
    def __init__(self, registry):
        super(Command, self).__init__(registry)
        self._diceengine = DiceEngine()
        self._error_message = "Invalid syntax"

    async def respond(self, message, command, client):
        tmp = await client.send_message(message.channel, 'Rolling..')

        nocalc = False
        if command.startswith("nocalc "):
            _, command = command.split(' ', 1)
            nocalc = True
        rolls, rollsum, rollstr = self._diceengine.parse_dice(command)
        
        if rolls is None:
            await client.edit_message(tmp, self._error_message)
            return

        if nocalc:
            await client.edit_message(tmp, str(rollsum))
        else:
            await client.edit_message(tmp, '{}={}'.format(rollstr, rollsum))
        
    @property
    def description(self):
        return "Roll some dice"

    @property
    def help(self):
        return "\n".join([
            "NdS - Roll N dice with S sides each",
            "NdSkZ - Roll N dice with S sides each, keeping the Z highest rolls",
            "NdS~kZ - Roll N dice with S sides each, keeping the Z lowest rolls",
            "Dice rolls can be combined with *, /, + and -, but there is no support for brackets"
        ])
    
    def set_config(self, dict):
        if "SyntaxError" in dict:
            self._error_message = dict["SyntaxError"]
    
    def get_config(self):
        return {}

class DiceEngine:
    class Tokens(Enum):
        NUMBER = 0
        TILDA = 1
        KEEP = 2
        DICE = 3
        ADD = 4
        SUB = 5
        MUL = 6
        DIV = 7
    
    class Rules(Enum):
        ROLL = 0
        MULDIV = 1
        ADDSUB = 2
        KD = 3
        K = 4
        D = 5
        N = 6

    class Token:
        def __init__(self, ttype, value):
            self.token_type = ttype
            self.value = value
    
    class Match:
        def __init__(self, rule, children):
            self.rule = rule
            self.children = children
    
    class Roll:
        def __init__(self, value, sides):
            self.value = value
            self.sides = sides
        
        def __str__(self):
            return "{}[d{}]".format(self.value, self.sides)

    _token_map = {
        '~': Tokens.TILDA,
        'k': Tokens.KEEP,
        'd': Tokens.DICE,
        '+': Tokens.ADD,
        '-': Tokens.SUB,
        '*': Tokens.MUL,
        '/': Tokens.DIV,
    }
    
    # We support a syntax as follows
    # */ - (MULDIV)
    # +- - (ADDSUB)
    # (~)k - Keep Highest (or Lowest if -) (KEEP)
    # NdN - N N sided dice rolls (D)
    # N - Number
    # Grammar is as follows
    # ROLL -> MULDIV ROLL | MULDIV
    # MULDIV -> ADDSUB * MULDIV | ADDSUB / MULDIV | ADDSUB
    # ADDSUB -> KD + ADDSUB | KD - ADDSUB | KD
    # KD -> D K 
    # K -> ~ k N | k N
    # D -> N d N 
    # N -> any number
    
    _rule_map = {
        Rules.ROLL: [
            [Rules.MULDIV]
        ],
        Rules.MULDIV: [
            [Rules.ADDSUB, Tokens.MUL, Rules.MULDIV],
            [Rules.ADDSUB, Tokens.DIV, Rules.MULDIV],
            [Rules.ADDSUB]
        ],
        Rules.ADDSUB: [
            [Rules.KD, Tokens.ADD, Rules.ADDSUB],
            [Rules.KD, Tokens.SUB, Rules.ADDSUB],
            [Rules.KD]
        ],
        Rules.KD: [
            [Rules.D, Rules.K],
            [Rules.D]
        ],
        Rules.K: [
            [Tokens.TILDA, Tokens.KEEP, Rules.N],
            [Tokens.KEEP, Rules.N]
        ],
        Rules.D: [
            [Rules.N, Tokens.DICE, Rules.N],
            
        ],
        Rules.N: [
            [Tokens.NUMBER]
        ]
    }
    
    def __init__(self):
        pass
    
    def parse_dice(self, string):
        tokens = self._tokenise(string)

        if tokens is None:
            return None, None, None

        ast, remaining = self._match(DiceEngine.Rules.ROLL, tokens)
        print("WEWLADWEWLAD")
        print(remaining, flush=True)
        if remaining:
            return None, None, None
            
        if ast is None:
            return None, None, None
        
        return self._parse_ast(ast)
    
    def _parse_ast(self, ast_node):
        if ast_node.rule == DiceEngine.Rules.ROLL:
            return self._parse_ast(ast_node.children[0])
        elif ast_node.rule == DiceEngine.Rules.MULDIV:
            if len(ast_node.children) > 1:
                left, leftsum, leftstr = self._parse_ast(ast_node.children[0])
                right, rightsum, rightstr = self._parse_ast(ast_node.children[2])

                if ast_node.children[1].rule == DiceEngine.Tokens.MUL:
                    return left + right, leftsum * rightsum, "({})*({})".format(leftstr, rightstr)
                else:
                    return left + right, leftsum / rightsum, "({})/({})".format(leftstr, rightstr)
            else:
                return self._parse_ast(ast_node.children[0])
        elif ast_node.rule == DiceEngine.Rules.ADDSUB:
            if len(ast_node.children) > 1:
                left, leftsum, leftstr = self._parse_ast(ast_node.children[0])
                right, rightsum, rightstr = self._parse_ast(ast_node.children[2])

                if ast_node.children[1].rule == DiceEngine.Tokens.ADD:
                    return left + right, leftsum + rightsum, "({})+({})".format(leftstr, rightstr)
                else:
                    return left + right, leftsum - rightsum, "({})-({})".format(leftstr, rightstr)
            else:
                return self._parse_ast(ast_node.children[0])
        elif ast_node.rule == DiceEngine.Rules.KD:
            if len(ast_node.children) > 1:
                rolls, rollsum, _ = self._parse_ast(ast_node.children[0])
                keepcount, highest, _ = self._parse_ast(ast_node.children[1])
                rolls = sorted(rolls, reverse=highest, key=operator.attrgetter('value'))[:keepcount]
                return rolls, sum([r.value for r in rolls]), '+'.join([str(r) for r in rolls])
            else:
                return self._parse_ast(ast_node.children[0])
        elif ast_node.rule == DiceEngine.Rules.K:
            count, _, _ = self._parse_ast(ast_node.children[-1])
            if ast_node.children[0].rule == DiceEngine.Tokens.TILDA:
                return count, False, None
            else:
                return count, True, None
        elif ast_node.rule == DiceEngine.Rules.D:
            count, _, _ = self._parse_ast(ast_node.children[0])
            sides, _, _ = self._parse_ast(ast_node.children[2])
            
            rolls = []
            for i in range(count):
                rolls.append(DiceEngine.Roll(random.randint(1, sides), sides))
            
            return rolls, sum([r.value for r in rolls]), '+'.join([str(r) for r in rolls])
        elif ast_node.rule == DiceEngine.Rules.N:
            return int(ast_node.children[0].children[0].value), None, None
        else:
            return None, None, None

    # Doesn't currently validate numbers correctly
    def _tokenise(self, string):
        splits = re.findall('[\d]+|[{}]'.format(re.escape(''.join(DiceEngine._token_map.keys()))), string.lower())
        return [DiceEngine.Token(DiceEngine._token_map.get(x, DiceEngine.Tokens.NUMBER), x) for x in splits]
    
    def _match(self, rule, tokens):
        # We're actually looking for a token, not a rule
        if tokens and rule == tokens[0].token_type:
            return DiceEngine.Match(rule, [tokens[0]]), tokens[1:]
        # Attempt all expansions, if this is a token, it'll fall through and try to iterate on an empty tuple, terminating early
        for expansion in DiceEngine._rule_map.get(rule, ()):
            remaining = tokens
            matched_subrules = []
            for subrule in expansion:
                match, remaining = self._match(subrule, remaining)
                if not match:
                    break # Couldn't match that subrule, try the next expansion
                matched_subrules.append(match)
            else:
                return DiceEngine.Match(rule, matched_subrules), remaining
        return None, None # No matching expansions