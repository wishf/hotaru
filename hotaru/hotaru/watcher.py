from abc import ABCMeta, abstractmethod, abstractproperty
from hotaru.plugin import Plugin

class Watcher(Plugin, metaclass=ABCMeta):
    @abstractmethod
    async def process(self, message, client):
        pass
    
    @abstractproperty
    def watches_self(self):
        pass