from setuptools import setup

setup(name='hotaru',
      version='0.0.1',
      description='A Discord bot',
      author='Matthew Summers',
      author_email='matt@wishf.co.uk',
      license='MIT',
      packages=['hotaru'],
      entry_points={
            'console_scripts': [
                  'hotaru = hotaru.runner:main',
            ],
            'hotaru.command': [
                  'roll = hotaru.plugins.roll:Roll',
                  'remember = hotaru.plugins.remember:Remember'
            ],
            'hotaru.persistence': [
                  'persistence-memory = hotaru.plugins.memorypersistence:MemoryPersistence'
            ]
      })